/*
	Client (GUI)
		>> is an application which creates requests for resources from a server.
		it will trigger an action, in an web dev contest, through a URL
		and wait for the response of the server

	Server
		>> is able to host and deliver resources requested by a client. A single server
		can handle multiple clients.

	Node.js

		>> is a runtime environment which allows us to create/develop 
		backend/server-side applicaiton with Javascript. By default, javascript was
		conceptualized solely to the front end.

	NodeJs is popular because it is one of the most performing environment
	for creating backend applciation with js.
	
	Familiarity - since node js si built and uses js as its language.

	Npm - helps you to create features in your application.
		- Node Package Manager is the largest registry for node packages.	
*/

//console.log("Hello World");

// require(); allows us to input packages
// Packages is a pieces of code that we can integrate in our applciaiton
// "http" is a default package from node js. allows us to use methods that let us create server.
// http - is a module.
// Modules are packages we imported. Are also objects that contains codes, methods or data.
// http module - allows us to create a server which is able to communicate with a client though the use of Hypertext Transfer Protocol
// http://localhost:4000 - http here is a protocol to client-server-communication.
// localhost:4000 is our server/application
// ctrl + c to terminate node in terminal and it's  a MUST!


let http = require("http");

/*
	createServer() is amthod from http module that alows us to handle requests and responses
	from a client and a server respectively
	
	It also takes the functions argument which is able to receive 2 objects.
	The request object which contains details of the request fromn the client.
	The response object which contains details of the response from the server.
	It ALWAYS receives the request object first before the response.	
*/

http.createServer(function(request,response){
/*
	request.url contains your endpoints.
	route is the process or way to respond differently to a request.
	URL are strings.
*/
	console.log(request.url);

if(request.url === "/"){
	/*
		response.writeHead(1st,2nd) allows us to add headers to our response. Headers are additonal information about our response.
		1st - it is the http status code. 200 means okay.	
		2nd - Content-Type pertains to the data type of our response.


	*/
	response.writeHead(200,{'Content-Type':'text/plain'});
	/*
	response.end() ends our response. Also able to send a message/data as a string.
	
*/

	response.end("Hello from our frist Server! This is from / endpoint.");

/*
	.listen() allows us to assign a port to our server. This will allows us server our
	index.js server in our local machine assigned to our port 4000. Tere are several tasks
	and processes on our computer/machine that run on different port numbers.
	
	it will listen to your machine.
*/

} else if(request.url === "/profile"){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hi! I'm Aries Hans Virtudao!");
}

/*
	Servers can response differently with different requests.

	We start our request with our URL. Client can start a different request with
	a different URL.
	http://localhost:400/ is not the same http://localhost:4000/profile
	/profile - '/' there is a URL end-point

	We can differentiate request by their endpoints, we should be able to respond 
	differently to different endpoints.
	
	Information about the URL endpoints of the request is in the request objects.

*/
}).listen(4000);

console.log("Server is running on LocalHost: 4000!");
// console.log(http);